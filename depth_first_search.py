# -*- coding: utf-8 -*-
"""
有一个层数为n（n<=1000)的数字三角形。
现有一只蚂蚁从顶层开始向下走，每走下一级，可向左下方向或右下方向走。
求走到底层后它所经过数字的总和的最大值。
【输入格式】
第一个整数为n,一下n行为各层的数字。
【输出格式】
一个整数，即最大值。
【输入样例 】
5
1
6 3
8 2 6
2 1 6 5
3 2 4 7 6
【输出样例】
23
【样例说明】
最大值=1+3+6+6+7=23
"""


class Solution:
    def __init__(self, N, a):
        self.sum_val = 0
        self.max = 0
        self.N = N
        self.a = a

    # dfs
    def dfs(self, i, j):
        self.sum_val += self.a[i][j]
        if i == self.N - 1:
            if self.sum_val > self.max:
                self.max = self.sum_val
            return
        for index in range(0, 2):
            self.dfs(i + 1, j + index)
            # 回溯
            self.sum_val -= a[i + 1][j + index]

    # 动态规划
    def dp(self):
        for i in range(self.N - 2, -1, -1):
            for j in range(0, i + 1):
                if self.a[i + 1][j] > self.a[i + 1][j + 1]:
                    a[i][j] += a[i + 1][j]
                else:
                    a[i][j] += a[i + 1][j + 1]
        return a[0][0]


if __name__ == '__main__':
    a = [[1], [6, 3], [8, 2, 6], [2, 1, 6, 5], [3, 2, 4, 7, 6]]
    solution = Solution(5, a)
    solution.dfs(0, 0)
    print(solution.max)
    print(solution.dp())
