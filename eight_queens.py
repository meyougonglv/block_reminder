# -*- coding: utf-8 -*-

"""
八皇后问题是一个以国际象棋为背景的问题：如何能够在 8×8 的国际象棋棋盘上放置八个皇后，
使得任何一个皇后都无法直接吃掉其他的皇后？为了达到此目的，任两个皇后都不能处于同一条横行、纵行或斜线上。
八皇后问题可以推广为更一般的n皇后摆放问题：这时棋盘的大小变为n1×n1，而皇后个数也变成n2。
而且仅当 n2 = 1 或 n1 ≥ 4 时问题有解。
"""


class DFS_Solution:
    def __init__(self):
        self.vis = [0] * 10
        self.result = 0

    def check(self, row, col):
        for i in range(1, row):
            if self.vis[i] == col:
                return False
            if abs(self.vis[i] - col) == abs(row - i):
                return False
        return True

    def dfs(self, row):
        if row > 8:
            self.result += 1
            return
        for i in range(1, 9):
            if self.check(row, i):
                self.vis[row] = i
                self.dfs(row + 1)
                self.vis[row] = 0


if __name__ == '__main__':
    solution1 = DFS_Solution()
    solution1.dfs(1)
    print(solution1.result)
