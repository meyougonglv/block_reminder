# -*- coding: utf-8 -*-
"""
题目是这样的：汉诺塔（Hanoi Tower），又称河内塔，源于印度一个古老传说。
大梵天创造世界的时候做了三根金刚石柱子，在一根柱子上从下往上按照大小顺序摞着64片黄金圆盘。
大梵天命令婆罗门把圆盘从下面开始按大小顺序重新摆放在另一根柱子上。
并且规定，任何时候，在小圆盘上都不能放大圆盘，且在三根柱子之间一次只能移动一个圆盘。
问应该如何操作？
简单点问题就是要把n个盘子从A柱子上移到C柱子上，盘子大小顺序不能颠倒，求操作步骤？
"""


class Solution:
    def __init__(self, count):
        self.count = count
        self.a = 'A'
        self.b = 'B'
        self.c = 'C'

    def hanoi(self):
        self.hanoi_core(self.count, self.a, self.b, self.c)

    def hanoi_core(self, n, a, b, c):
        if n == 1:
            self.move(a, c)
        else:
            # 将n-1个盘子从a移动到b
            self.hanoi_core(n - 1, a, c, b)
            # 将剩下一个盘从a直接移动到c
            self.move(a, c)
            # 将n-1个盘子从b移动到c
            self.hanoi_core(n - 1, b, a, c)

    def move(self, left, right):
        print('move one disk: %s ---> %s' % (left, right))


"""
若加上一个限制条件，圆盘只能在相邻柱子之间移动，又如何解决？
假设a, b, c并排，b在中间，即a, c不相邻，把a上的一个圆盘移动到c上必须先移至b，然后再移动到c。
"""


class SolutionAdvance:
    def __init__(self, count):
        self.count = count
        self.a = 'A'
        self.b = 'B'
        self.c = 'C'

    def hanoi(self):
        self.hanoi_core(self.count, self.a, self.b, self.c)

    def hanoi_core(self, n, a, b, c):
        if n == 1:
            self.move(a, b)
            self.move(b, c)
        else:
            self.hanoi_core(n - 1, a, b, c)
            self.move(a, b)
            self.hanoi_core(n - 1, c, b, a)
            self.move(b, c)
            self.hanoi_core(n - 1, a, b, c)

    def move(self, left, right):
        print('move one disk: %s ---> %s' % (left, right))


"""
最近点对问题
给出二维平面上的n个点，求其中最近的两个点的距离。

"""
import math


class P2PSolution:

    def __init__(self, point_array):
        self.points = point_array

    def get_min_dis(self):
        return self.get_min_dis_core(self.points)

    def get_min_dis_core(self, s):
        mid_index = len(s) // 2
        left = s[:mid_index]
        right = s[mid_index:]

        if len(left) > 2:
            lmin = self.get_min_dis_core(left)
        else:
            lmin = left
        if len(right) > 2:
            rmin = self.get_min_dis_core(right)
        else:
            rmin = right

        dis_l = self.get_dis(lmin[0], lmin[1]) if len(lmin) == 2 else float('inf')
        dis_r = self.get_dis(rmin[0], rmin[1]) if len(rmin) == 2 else float('inf')

        d = min(dis_l, dis_r)

        mid_x = (left[-1][0] + right[0][0]) / 2.0

        mid_min = []
        for i in left:
            if mid_x - i[0] <= d:
                for j in right:
                    if abs(i[0] - j[0]) <= d and abs(i[1] - j[1]) <= d:
                        if self.get_dis(i, j) <= d:
                            mid_min.append([i, j])

        if mid_min:
            dic = []
            for i in mid_min:
                dic.append({self.get_dis(i[0], i[1]): i})
            dic.sort(key=lambda x: x.keys())
            return dic[0].values()[0]
        elif dis_l > dis_r:
            return rmin
        else:
            return lmin

    def get_dis(self, a, b):
        return math.sqrt(pow((a[0] - b[0]), 2) + pow((a[1] - b[1]), 2))


if __name__ == '__main__':
    Solution(3).hanoi()
    SolutionAdvance(3).hanoi()
    s = [(0, 1), (3, 2), (4, 3), (5, 1), (1, 2), (2, 1), (6, 2), (7, 2), (8, 3), (4, 5), (9, 0), (6, 4)]
    s.sort(cmp=lambda x, y: cmp(x[0], y[0]))
    result = P2PSolution(s).get_min_dis()
    print(result)
