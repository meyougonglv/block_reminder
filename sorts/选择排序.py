def select_sort(list):
    length = len(list)
    for i in range(length):
        k = i
        for j in range(k + 1, length):
            if list[j] < list[k]:
                k = j
        if k != i:
            list[i], list[k] = list[k], list[i]
    return list


if __name__ == '__main__':
    list = [1, 2, 4, 5, 8, 7, 3, 6]
    print(select_sort(list))