def bubble_sort(list):
    length = len(list)
    for i in range(length):
        for j in range(length-1-i):
            if list[j] > list[j+1]:
                list[j], list[j+1] = list[j+1], list[j]
            j += 1
    return list


if __name__ == '__main__':
    list = [1, 2, 4, 5, 8, 7, 3, 6]
    print(bubble_sort(list))