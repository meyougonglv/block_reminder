def quick_sort(l):
    assert (type(l) == type(['']))
    length = len(l)
    if length == 0 or length == 1:
        return l
    if len(l) <= 1:
        return l
    left = [i for i in l[1:] if i > l[0]]
    right = [i for i in l[1:] if i <= l[0]]
    return quick_sort(left) + [l[0], ] + quick_sort(right)


def quick_sort2(l, s, e):
    length = len(l)
    if length == 0 or length == 1:
        return l

    def partition(m_list, start, end):
        pivot = m_list[start]
        while start < end:
            while end > start and m_list[end] < pivot:
                end -= 1
            m_list[start] = m_list[end]
            while end > start and m_list[start] > pivot:
                start += 1
            m_list[end] = m_list[start]
        m_list[start] = pivot
        return start

    if s < e:
        m = partition(l, s, e)
        quick_sort2(l, s, m - 1)
        quick_sort2(l, m + 1, e)
    return l


def quick_sort3(l, s, e):
    if s >= e:
        return

    m = s
    for i in range(s + 1, e + 1):
        if l[i] < l[s]:
            m += 1
            l[m], l[i] = l[i], l[m]
            print(l)
    l[m], l[s] = l[s], l[m]
    print(l)

    quick_sort3(l, 0, m - 1)
    quick_sort3(l, m + 1, e)


if __name__ == '__main__':
    list = [6, 5, 3, 1, 8, 7, 2, 4]
    # quick_sort2(list, 0, 7)
    quick_sort3(list, 0, 7)
    print(list)
