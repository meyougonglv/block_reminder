def insert_sort(list):
    length = len(list)
    if length <= 1:
        return list
    for i in range(1, length):
        temp = list[i]
        j = i - 1
        while list[j] > temp and j > 0:
            list[j + 1] = list[j]
            j -= 1
        list[j + 1] = temp
    return list


if __name__ == '__main__':
    list = [1, 2, 4, 5, 8, 7, 3, 6]
    print(insert_sort(list))
