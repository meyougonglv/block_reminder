# -*- coding: utf-8 -*-


def heapSort(L):
    assert (type(L) == type(['']))
    length = len(L)
    if length == 0 or length == 1:
        return L

    def sift_down(L, start, end):
        root = start
        while True:
            child = 2 * root + 1
            if child > end: break
            if child + 1 <= end and L[child] > L[child + 1]:
                child += 1
            if L[root] > L[child]:
                L[root], L[child] = L[child], L[root]
                root = child
            else:
                break

# 初始化小顶堆
    for start in range((len(L) - 2) / 2, -1, -1):
        sift_down(L, start, len(L) - 1)

#  交换堆顶和末尾元素，并重新调整除末尾已经有序的值外的list成小顶堆
    for end in range(len(L) - 1, 0, -1):
        L[0], L[end] = L[end], L[0]
        sift_down(L, 0, end - 1)
    return L


if __name__ == '__main__':
    list = [1, 2, 4, 5, 8, 7, 3, 6]
    print(heapSort(list))
