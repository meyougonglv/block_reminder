# THIS IS A REMINDER FOR ALGORITHMS

## divide and conquer  

[五大常用算法-分治法](https://gitlab.com/meyougonglv/block_reminder/wikis/Summary)

## dynamic programming

[五大常用算法-动态规划](https://gitlab.com/meyougonglv/block_reminder/wikis/%E5%8A%A8%E6%80%81%E8%A7%84%E5%88%92)

## back tracking

[五大常用算法-回溯法](https://gitlab.com/meyougonglv/block_reminder/wikis/%E5%9B%9E%E6%BA%AF%E6%B3%95)

## greedy algorithm

[五大常用算法-贪心算法](https://gitlab.com/meyougonglv/block_reminder/wikis/%E8%B4%AA%E5%BF%83%E7%AE%97%E6%B3%95)

## Branch and Bound Method

[五大常用算法-分支限界法](https://gitlab.com/meyougonglv/block_reminder/wikis/%E5%88%86%E6%94%AF%E9%99%90%E7%95%8C%E6%B3%95)

## PCA AND LDA

[机器学习知识点拾遗-PCA&LDA](https://gitlab.com/meyougonglv/block_reminder/blob/master/machine%20learning%20reminder.md#pca-%E5%92%8C-lda)

.py file contains classical problem and solutions.